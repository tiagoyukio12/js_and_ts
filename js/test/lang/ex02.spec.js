function createUser(name, email, password, passwordConfirmation) {
  if (name.length <= 0 || name.length > 64) {
    throw new Error('validation error: invalid name size');
  }

  if (!email.match(/[a-z]+@[a-z]+\.com(\.[a-z]{2})/)) {
    throw new Error('validation error: invalid email format');
  }

  if (!password.match(/^[a-z0-9]{6,20}$/)) {
    // ^ and $ to define max length match
    throw new Error('validation error: invalid password format');
  }

  if (password !== passwordConfirmation) {
    throw new Error('validation error: confirmation does not match');
  }
}

describe(createUser, () => {
  // Valid fields
  const name = 'Tiago';
  const email = 'tiago@usp.com.br';
  const password = 'mac0475';
  const passwordConfirmation = 'mac0475';
  
  test("Doesn't throw error if all fields are valid", () => {
    expect(() => createUser(name, email, password, passwordConfirmation)).not.toThrowError();
  })

  test('Throws error if name is too short', () => {
    expect(() => createUser('', email, password, passwordConfirmation)).toThrowError();
  })

  test('Throws error if name is too long', () => {
    const longName = '0123456789012345678901234567890123456789012345678901234567890123456789';
    expect(() => createUser(longName, email, password, passwordConfirmation)).toThrowError();
  })

  test('Throws error if email is invalid', () => {
    expect(() => createUser(name, 'tiago@usp.com', password, passwordConfirmation)).toThrowError();
  })

  test('Throws error if password is too short', () => {
    const invalidPassword = '12345';
    expect(() => createUser(name, email, invalidPassword, invalidPassword)).toThrowError();
  })

  test('Throws error if password is too long', () => {
    // This test only works if regex is /^[a-z0-9]{6,20}$/
    const invalidPassword = '012345678901234567890123456789';
    expect(() => createUser(name, email, invalidPassword, invalidPassword)).toThrowError();
  })

  test('Throws error if password has invalid character', () => {
    const invalidPassword = '123456789!';
    expect(() => createUser(name, email, invalidPassword, invalidPassword)).toThrowError();
  })

  test('Throws error if password confirmation mismatches', () => {
    expect(() => createUser(name, email, password, 'MAC0475')).toThrowError();
  })
});
