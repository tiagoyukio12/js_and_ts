function objectFromArrays(keys, values) {
  const finalObject = {[keys[0]]: values[0], [keys[1]]: values[1], [keys[2]]: values[2]};
  return finalObject;
}

test('Creating objects from arrays', () => {
  const keys = ['name', 'email', 'username'];
  const values = ['Bruno', 'bruno@mail.com', 'bruno'];

  const finalObject = {
    name: 'Bruno',
    email: 'bruno@mail.com',
    username: 'bruno'
  };

  expect(objectFromArrays(keys, values)).toEqual(finalObject);
});

function reverse(a, b) {
  if (a > b)
    return -1;
  if (a < b)
    return 1;
}

function indexer(val, index, array) {
  return (index + 1) + '. ' + val;
}

function shorterThan6(word) {
  return word.length < 6;
}

describe('array functions', () => {
  test('reversed indexed arrays', () => {
    const names = ['Marina', 'Camila', 'Alberto', 'Felipe', 'Mariana'];

    const test = names.sort(reverse).map(indexer);
    const correct = [
      '1. Marina',
      '2. Mariana',
      '3. Felipe',
      '4. Camila',
      '5. Alberto'
    ];
    expect(test).toEqual(correct);
  });

  test('filtering lists', () => {
    const techComps = [
      'microsoft',
      'google',
      'apple',
      'ibm',
      'amazon',
      'facebook'
    ];

    const shortNames = techComps.filter(shorterThan6);
    const correctShortNames = ['apple', 'ibm'];

    expect(shortNames).toEqual(correctShortNames);
  });
});
