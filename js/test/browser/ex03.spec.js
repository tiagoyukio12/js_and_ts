const test = require('ava');

function scaffoldStructure(document, data) {
  const orderedData = data.sort(function(a, b) {
    if (a.name < b.name) { return -1; }
    if (a.name > b.name) { return 1; }
    return 0;
  })

  let ul = document.createElement('ul');

  for (let i = 0; i < orderedData.length; i++) {
    let li = document.createElement('li');
    let div = document.createElement('div');
    div.innerHTML = '<b class="name">' + orderedData[i]['name'] + '</b>';
    div.innerHTML += ' - ';
    div.innerHTML += '<u>' + orderedData[i]['email'] + '</u>';

    li.appendChild(div);
    ul.appendChild(li);
  }
  document.body.appendChild(ul);
}

test('the creation of a page from scratch', t => {
  const data = [
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);

  const unorderedList = document.querySelector('ul');
  t.truthy(unorderedList, 'Unordered list exists')

  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes.length, data.length, 'has elements of class name');
  
  const items = unorderedList.childNodes;
  let arrItems = [];
  for (let i = 0; i < items.length; i++) {
    arrItems.push(items[i].innerHTML);
  }
  let sortedItems = arrItems.concat().sort();
  t.deepEqual(arrItems, sortedItems, 'list items are ordered alphabetically');

  let allMatch = true;
  for (let i = 0; i < arrItems.length; i++) {
    if (!arrItems[i].match(/<div><b class="name">.+<\/b> - <u>.+<\/u><\/div>/)) {
      allMatch = false;
      break;
    }
  }
  t.is(allMatch, true, 'list elements follow template')
});
